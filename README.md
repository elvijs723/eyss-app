# EyssApp    (PICTURES below)

This is small forum system, consisting of (this) Angular Javascript client, who talks with backend **Java Spring Boot** through HttpClient REST calls.
There are some bad practices in (this) Angular app, like no custom observables, instead just re-loading of page (bad), alerts are used instead of pop-up tab implementation on errors. 

**But** otherwise the system is almost fully functional, the only major downside is that **when JWT Authentication Token expires** there isn't implemented specific error coming from back-side (Java) 
who'd trigger the request for re-login, when token is expired, meaning you have to re-log-in, for rights to make POST requests.

Client-side session is kept live with JWT (JSON Web Token), who is kept in local storage or window storage and used in interceptor for HttpClient calls to back-end; 
The JWT user-information decoding doesn't take place on (this) app, instead there is bunch of user information stored in local/window storage with JWT token.  

## Development server

Serves typescript converted to javascript to client, from a webserver.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

No testing has been done.

## Running end-to-end tests

No testing has been done.

## Further info
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.0.
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

![alt text](https://bytebucket.org/elvijs723/eyss-app/raw/2a034e397bc0b4f7dba4f973cf2b71a5b3253386/snippets/snip1.JPG)
![alt text](https://bytebucket.org/elvijs723/eyss-app/raw/2a034e397bc0b4f7dba4f973cf2b71a5b3253386/snippets/snip2.JPG)
![alt text](https://bytebucket.org/elvijs723/eyss-app/raw/2a034e397bc0b4f7dba4f973cf2b71a5b3253386/snippets/snip3.JPG)
![alt text](https://bytebucket.org/elvijs723/eyss-app/raw/2a034e397bc0b4f7dba4f973cf2b71a5b3253386/snippets/snip4.JPG)
