//angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule } from '@angular/forms';


//core
import { AppRoutingModule } from './core/routing.module';
import { TokenStorage } from './core/token.storage';
import { Interceptor } from './core/interceptor';

//services
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';

//components
import { AppComponent } from './app.component';
import { RegComponent } from './components/user/reg/reg.component';
import { AuthComponent } from './components/user/auth/auth.component';
import { UserComponent } from './components/user/user.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ArticleComponent } from './components/article/article.component';
import { CommentComponent } from './components/comment/comment.component';
import { PostArticleComponent } from './components/post-article/post-article.component';
import { UpdateComponent } from './components/update/update.component';





@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HomeComponent,
    AuthComponent,
    RegComponent,
    NavbarComponent,
    ArticleComponent,
    CommentComponent,
    PostArticleComponent,
    UpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true
    },
    AuthService,
    UserService,
    TokenStorage
  ],
  bootstrap: [AppComponent, NavbarComponent]
})
export class AppModule { }
