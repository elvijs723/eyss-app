import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { UserService } from '../../services/user.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Comment } from '../../models/comment.model';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  private totalPages: number;
  private page: number;
  private comments = [];
  private article = {};
  private comment: Comment;
  private pageTrio = {};
  private articleId: number;

  private commentValue;
  //private id: string;
  //private article;

  constructor(private articleService: ArticleService, private activatedroute: ActivatedRoute, private userService: UserService,
    private router: Router) {
    //this.pageTrio = { 'first': this.page, 'second': this.page + 1, 'third': this.page + 2 };
  }

  ngOnInit() {
    this.page = 1;
    this.articleId = this.activatedroute.snapshot.params['id'];
    this.getArticle();
    this.getComments();
    this.comment = new Comment();
  }

  getArticle() {
    this.articleService.getArticle(this.articleId)
      .subscribe(
      data => {
        this.article = data;
        //this.comment.articleId = data['content']['id'];
      })
  }
    getComments(){
      this.articleService.getComments(this.articleId, this.page-1)
        .subscribe(
        data => {
          this.comments = data['content'];
          this.totalPages = data['totalPages'];
          
        }
          )
    }

    goToUser(user) {
      this.userService.navigateToUser(user);
      //this.router.navigate(['']);
      //this.router.navigate(['/user/','zodiac']);
    }


    onSubmit() {
      this.comment.articleId = this.article['id'];
      console.log("Submitting this - " + this.comment);
      this.articleService.submitComment(this.comment)
        .subscribe(
        data => {
          console.log(data);

          window.location.reload();
          alert(data.message);
        },
        err => {
          alert(err.error.message);
        }
        );
    }

    getNext() {
      if (this.page == this.totalPages) alert("this is the last page");
      else {
        this.page += 1;
        this.getComments();

      }
    }
    getPrev() {
      if (this.page == 1) alert("this is the first page");
      else {
        this.page -= 1;
        this.getComments();

      }
    }
}


