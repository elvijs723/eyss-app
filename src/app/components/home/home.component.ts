import { Component, OnInit } from '@angular/core';

import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { ArticleService } from '../../services/article.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  currentUser: User;
  users: User[] = [];
  private totalPages: number;
  private page: number;
  private articles: Array<any>;

  //users: User[] = [];
  constructor(private userService: UserService, private articleService: ArticleService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    //this.loadAllUsers();
    this.page = 1;
    this.listArticles();
  }

  listArticles() {
    this.articleService.getArticles(this.page-1).subscribe(
      data => {
        this.articles = data['content'];
        this.totalPages = data['totalPages'];
      },
      error => {
        console.log("");
      }
    )
  }

  getNext() {
    if (this.page == this.totalPages) alert("this is the last page");
    else {
      this.page += 1;
      this.listArticles();
     
    }
  }
    getPrev(){
      if (this.page == 1) alert("this is the first page");
      else {
        this.page -= 1;
        this.listArticles();
 
      }
    }
  }

  //   data=>  { this.pages = new Array(data['totalPages'])}

  //deleteUser(id: number) {
  //  this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
  //}

  //private loadAllUsers() {
  //  this.userService.getAll().subscribe(users => { this.users = users; });
  //}



//this.loading = true;
//this.authenticationService.login(this.model.username, this.model.password)
//  .subscribe(
//  data => {
//    this.router.navigate([this.returnUrl]);
//  },
//  error => {
//    this.alertService.error(error);
//    this.loading = false;
//  });
