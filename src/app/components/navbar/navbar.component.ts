import { Component, OnInit } from '@angular/core';

import { Auth } from '../../models/auth.model';
import { User } from '../../models/user.model';
import { AuthService } from '../../services/auth.service';
import { TokenStorage } from '../../core/token.storage';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  eyssUser: User;
  isAuth = false;
  //userData: Auth;
  //constructor(private authorization : AuthorizationService) { }
  constructor(private authService: AuthService, private token: TokenStorage) {
    this.eyssUser = JSON.parse(window.sessionStorage.getItem('eyss-user'));
  }
  
  ngOnInit() {
    if (this.eyssUser != null) {
      if (this.eyssUser.authority == 0 || this.eyssUser.authority == 1) { this.isAuth = true; }
    }
    else this.isAuth = false;
    console.log(this.isAuth);
    //this.userData = new Auth(); 
    //this.userData = this.authorization.getAuth();
    //console.log(this.userData);
    //this.authorization.getAuth.subscribe(
    //  {
    //    data=>{
    //      auth = data;
    //    }
    //  })
    //this.auth = this.authorization.getAuth();
    //console.log(this.authorization.getRole);
    //console.log(this.authorization.getUser);
  }
  //isWorking() {
  //  if (this.eyssUser == null) return false;
  //  else return true;
  //}

  logout() {
    this.authService.logout();
    //console.log(this.token.getToken());
    //window.sessionStorage.clear;
  }
  //isAuth() {
  //  //console.log("NOW");
    
   
  //}

}
