import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../../services/article.service';
import { Article } from '../../models/article.model';

@Component({
  selector: 'app-post-article',
  templateUrl: './post-article.component.html',
  styleUrls: ['./post-article.component.scss']
})
export class PostArticleComponent implements OnInit {
  private article: Article;

  constructor(private articleService: ArticleService, private router: Router) { }

  ngOnInit() {
    this.article = new Article();
  }

  onSubmit() {
    console.log("Submitting this - " + this.article);
    this.articleService.submitArticle(this.article)
      .subscribe(
      resp => {
        alert(resp.message);
        this.router.navigate(['']);
      },
      resp => {
        //console.log(resp.error.errors[0].defaultMessage);
        //alert(resp.error.errors[0].defaultMessage);
        alert(resp.error.message);
      }
      )
  }

}
