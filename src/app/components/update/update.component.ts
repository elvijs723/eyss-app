import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  private text:string;
  constructor(private userService: UserService, private router: Router, private location: Location) { }

  ngOnInit() {
  }

  onSubmit() {
    this.userService.updateInfo(this.text)
      .subscribe(
      ok => {
        ok = ok;
        this.router.navigate(['user/']);
        this.location.back();
      })
  }

}
