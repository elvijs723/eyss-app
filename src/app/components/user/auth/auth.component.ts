//angular
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
//core
import { TokenStorage } from '../../../core/token.storage';
//services
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
//import { AuthorizationService } from '../../../core/authorization.service';
//components
import { LoginUser } from '../../../models/login.user.model';
import { User } from '../../../models/user.model';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  loginUser = new LoginUser();
  eyssUser: User;

  constructor(private authService: AuthService, private token: TokenStorage, private router: Router) {
    this.eyssUser = JSON.parse(window.sessionStorage.getItem('eyss-user'));
  }

  isLoginError = false;

  ngOnInit() {
  }

  login() {
    this.token.signOut();
    //this.authorization.clearUser();
    this.authService.attemptAuth(this.loginUser).subscribe((data: any) => {
      console.log(data);
      this.token.saveToken(data.token);
      let user = { 'userName': data.userName, 'authority': this.getAuthority(data.role) };
      window.sessionStorage.setItem('eyss-user', JSON.stringify(user));
      //this.authorization.setUpUser(data.token);
      //this.authorization.display();
      //localStorage.setItem('token', data.token);
      //this.router.navigate(['/home']);
      window.location.reload();
      
    },
      err => {
        alert(err.error.message)
      });
  }

  getAuthority(s: string) {
    let num: number;
    if (s == "[ROLE_CLIENT]") num = 1;
    else if (s == "[ROLE_ADMIN]") num = 0;
    else num = null;
    return num;
  }

  //logout() {
  //  this.token.signOut();
  //  console.log(this.token.getToken);
  //}

}
