import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../../services/auth.service';
import { RegUser } from '../../../models/reg.user.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-reg',
  templateUrl: './reg.component.html',
  styleUrls: ['./reg.component.scss']
})
export class RegComponent implements OnInit {

  regUser = new RegUser();

  submitted = false;
  message: string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    this.authService.attemptReg(this.regUser)
      .subscribe(
      data => {
        alert(data.message);
        this.router.navigate(['']);
      },
      err => {
        alert(err.error.message);
      }
    );
  }

}
