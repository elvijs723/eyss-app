//angular
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

//import { AuthService } from '../../services/auth.service';

//core
import { TokenStorage } from '../../core/token.storage';
//services
import { UserService } from '../../services/user.service';
import { ArticleService } from '../../services/article.service';
//components
import { User } from '../../models/user.model';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  private page: number;
  private totalPages: number;
  private user = {};
  private userName;
  private articles: Array<any>;

  private eyssUser: User;
  private isHisProfile = false;
  private isAdmin = false;
  private snapshotParams: any;

  constructor(private userService: UserService, private token: TokenStorage, private route: ActivatedRoute,
    private articleService: ArticleService) {
      this.eyssUser = JSON.parse(window.sessionStorage.getItem('eyss-user'));
  }

  

  ngOnInit() {
   
    this.route.params.subscribe(params => { console.log(params); this.setUpUser(params.name); });
    this.page = 1;

  }

  setUpUser(name) {
    //this.userName = this.route.snapshot.params['name'];
    this.getUser(name);
    if (this.eyssUser != null) {
      if (this.eyssUser.userName == name) {
        this.isHisProfile = true;
      }
      if (this.eyssUser.authority == 0) {
        this.isAdmin = true;
      }
    }
  }

  getUser(userName) {
    this.userService.getUserInfo(userName)
      .subscribe(
      data => {
        this.user = data;
        this.getAllUserArticles(data['id'], this.page);
        //console.log(this.user)
      }
      )
  }

  getAllUserArticles(id:number,page:number) {
    this.articleService.getUserArticlesById(id, page-1)
      .subscribe(
      data => {
        this.articles = data['content'];
        this.totalPages = data['totalPages'];
      }
      )
  }

   goToUser(user) {
      this.userService.navigateToUser(user);
      //this.router.navigate(['']);
      //this.router.navigate(['/user/','zodiac']);
  }

   goToArticle(id) {
     this.articleService.navigateToArticle(id);
   }

   deleteArticle(id) {
     this.userService.deleteArticle(id)
       .subscribe(
       data => {
         window.location.reload();
       }
       )
   }

   deleteUser(id) {
     this.userService.deleteUser(id)
       .subscribe(
       data => {
         window.location.reload();
       })
   }
  //getFloor() {
  //  console.log("works");
  //  this.userService.getsmth()
  //    .subscribe(data => {
  //      this.data = data;
  //    });
  //}
  //re() {
  //  this.token.signOut();
  //}

  //re2() {
  //  this.userService.getsmth2()
  //    .subscribe(data => {
  //      this.data = data;
  //    });
  //}

   getNext() {
     if (this.page == this.totalPages) alert("this is the last page");
     else {
       this.page += 1;
       this.getAllUserArticles(this.user['id'], this.page);

     }
   }
   getPrev() {
     if (this.page == 1) alert("this is the first page");
     else {
       this.page -= 1;
       this.getAllUserArticles(this.user['id'], this.page);

     }
   }
}
