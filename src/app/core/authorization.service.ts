import { Injectable } from '@angular/core';

import { Auth } from './../models/auth.model';
//import * as jwt-decode from '@angular/jwt-decode';
import * as JWT from 'jwt-decode';

const ROLE_KEY = 'eyss-role';
const USER_KEY= "eyss-user"


@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  private model = {};
  //private token: string = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbjIiLCJhdXRoIjpbeyJhdXRob3JpdHkiOiJST0xFX0FETUlOIn1dLCJleHAiOjE1MjgyOTg4OTB9.smQ1tSd1oorKh9aPbmpz8vsJxran - EiZp8gROaSCXCE';
  //private model = {};
  
  auth: Auth = new Auth();
  constructor() { }

  onInit() {
    //console.log(JWT(this.token);
  }

  display() {
  }

  getAuth() {
    this.auth.user = this.getUser;
    this.auth.role = this.getRole;
    this.auth.auth = true;
    return this.auth;
  }

  setUpUser(token: string) {
    var tokenObj = JWT(token);
    this.storeUser(tokenObj['sub']);
    this.storeRole(tokenObj['auth'][0]);
    this.auth.auth = true;
  }
  clearUser() {
    this.removeRole();
    this.removeUser();
    window.sessionStorage.clear();
    this.auth.auth = false;
  }

  storeUser(user: string) {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, user);
  }
  removeUser() {
    window.sessionStorage.removeItem(USER_KEY);
  }
  storeRole(role: string) {
    window.sessionStorage.removeItem(ROLE_KEY);
    window.sessionStorage.setItem(ROLE_KEY, role);
  }
  removeRole() {
    window.sessionStorage.removeItem(ROLE_KEY);
  }
  getUser() {
    return window.sessionStorage.getItem(USER_KEY);
  }
  getRole() {
    return window.sessionStorage.getItem(ROLE_KEY);
  }
}
