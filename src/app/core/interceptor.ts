import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { TokenStorage } from '../core/token.storage'
//import 'rxjs/add/observable/throw';
//import 'rxjs/add/operator/catch';

const TOKEN_HEADER_KEY = "Authorization";

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(private token: TokenStorage, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    console.log("interceptss ");

    let modifiedReq = req;
    if (this.token.getToken() != null) {
      modifiedReq = req.clone({
        headers: req.headers.set(TOKEN_HEADER_KEY,
        'Bearer '+ this.token.getToken())
      })
    }
    return next.handle(modifiedReq);
    
    //req.clone({ headers: req.headers.set() })

    //console.lol("dony");
    
  }

}
