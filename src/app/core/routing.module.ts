import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from '../components/user/user.component';
import { HomeComponent } from '../components/home/home.component';
import { AppComponent } from '../app.component';
import { RegComponent } from '../components/user/reg/reg.component';
import { ArticleComponent } from '../components/article/article.component';
import { PostArticleComponent } from '../components/post-article/post-article.component';
import { UpdateComponent } from '../components/update/update.component';



const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'user/:name/update',
    component: UpdateComponent
  },
  {
    path: 'user/:name',
    component: UserComponent
  },
  {
    path: 'postArticle',
    component: PostArticleComponent
  },
  {
    path: 'register',
    component: RegComponent
  },
  {
    path: 'article/:id',
    component: ArticleComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
