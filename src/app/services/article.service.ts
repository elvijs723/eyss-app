import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Comment } from '../models/comment.model'
import { Article } from '../models/article.model';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  headers: HttpHeaders = new HttpHeaders();

  readonly articleUrl: "http://localhost:8080/article/";

  constructor(private http: HttpClient, private router: Router) { }
  onInit() {
    this.headers.append('Content-Type', 'application/json');
  }

  getArticles(page:number) {
    return this.http.get('http://localhost:8080/article/list/'+ page);
  }
  getArticle(id:number) {
    return this.http.get('http://localhost:8080/article/' + id);
  }

  getUserArticlesById(id, page: number) {
    console.log("Here");
    return this.http.get('http://localhost:8080/article/list/' + id + '/' + page);
  }

  getComments(id, page) {
    return this.http.get('http://localhost:8080/comment/' + id + '/' + page);
  }

  navigateToArticle(id) {
    this.router.navigate(['article', id]);
  }

  submitComment(comment : Comment) {
    return this.http.post<any>('http://localhost:8080/comment', comment);
  }

  submitArticle(article: Article) {
    return this.http.post<any>('http://localhost:8080/article', article);
  }
  
}
