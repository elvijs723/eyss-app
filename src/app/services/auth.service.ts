import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user.model';
import { LoginUser } from '../models/login.user.model';
import { RegUser } from '../models/reg.user.model';
import { TokenStorage } from '../core/token.storage';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  readonly authUrl: "http://localhost:8080/auth";
  headers: HttpHeaders = new HttpHeaders();

  constructor(private http: HttpClient, private token : TokenStorage, private router : Router) { }
  onInit() {
    this.headers.append('Content-Type', 'application/json');
  }

  //login(username: string, password: string) {
  //  return this.http.post<User>(this.baseUrl, { username, password })
  //    .do(res=>this.setSession)
  //    .shareReplay();
  //}
  //reqHeader = new HttpHeaders({ 'No-Auth': 'True' });
  attemptAuth(logInUser) {
      //var reqHeader = new HttpHeaders({ 'Content-Type': 'application/json' }, { 'Response-Type': 'text' });

   
    return this.http.post<LoginUser>('http://localhost:8080/auth/login', logInUser, { headers: this.headers });
  
    //return this.http.get('http://localhost:8080/v1/floor1/office1');
  }

  attemptReg(regUser: RegUser) {
    return this.http.post<any>('http://localhost:8080/auth/register', regUser, { headers: this.headers });
  }

  logout() {
    this.token.signOut();
    console.log(this.token.getToken());
    window.sessionStorage.removeItem('eyss-user');
    window.location.reload();
  }

}
