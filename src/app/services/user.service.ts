import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User } from '../models/user.model';
import { Router } from '@angular/router';  

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private router : Router) { }
  private userUrl = 'http://localhost:8080/user/';


  public getUsers() {
    return this.http.get<User[]>(this.userUrl);
  }
  public createUser(user) {
    return this.http.post<User>(this.userUrl, user);
  }


  attemptAuth(username, password) {
    const credentials = { username: username, password: password };
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');

    //var reqHeader = new HttpHeaders({ 'Content-Type': 'application/json' }, { 'Response-Type': 'text' });

    return this.http.post<any>('http://localhost:8080/auth/signin', credentials, { headers });
  }
    getsmth(){
      return this.http.get('http://localhost:8080/v1/floor1/office1');
  }

    //getsmth2() {
    //  return this.http.get('http://localhost:8080/users/51');
    //}

    navigateToUser(user) {
      this.router.navigate(['/user/'+user]);
    }


    getUserInfo(user) {
      return this.http.get(this.userUrl + user);
    }

    updateInfo(text) {
      return this.http.put('http://localhost:8080/user', text);
    }

    deleteArticle(id) {
      return this.http.delete('http://localhost:8080/article/' + id);
    }

    deleteUser(id) {
      return this.http.delete('http://localhost:8080/user/' + id);
    }

}
